#include <image.h>
#include <stdint.h>
#include <stdlib.h>

#define PIXEL_SIZE sizeof(struct pixel)



struct image create_image(uint64_t width, uint64_t height) {
    struct pixel* pixels = (struct pixel*) malloc(PIXEL_SIZE * width * height);
    if (pixels == NULL) {
        return (struct image){
                .height = 0,
                .width = 0,
                .data = pixels};
    }

    return (struct image){
            .height = height,
            .width = width,
            .data = pixels};

}

void delete_image(struct image* image) {
    free(image->data);
    image->data = NULL;
}
