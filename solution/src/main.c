#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Invalid arguments. Correct usage: <source-image> <transformed-image> <angle>\n");
        return -1;
    }

    char* input_filename = argv[1];
    char* output_filename = argv[2];

    int64_t angle = strtoll(argv[3], NULL, 10);

    FILE* input = fopen(input_filename, "rb");

    if (!input) {
        fprintf(stderr, "Error while opening the input file\n");
        return -1;
    }

    struct image img = {0};
    enum read_status read = from_bmp(input, &img);

    if (read != READ_OK) {
        fprintf(stderr, "Error while reading the file\n");
        delete_image(&img);
        return -1;
    }


    rotate(&img, angle);

    FILE* output = fopen(output_filename, "wb");
    if (!output) {
        fprintf(stderr, "Error while opening the output file\n");
        delete_image(&img);
        return -1;
    }

    if (to_bmp(output, &img) != WRITE_OK) {
        fprintf(stderr, "Error while writing into the file");
        delete_image(&img);
        return -1;
    }
    printf("Rotated the image successfully");
    delete_image(&img);
    return 0;
}
