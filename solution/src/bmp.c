#include <bmp.h>
#include <stdint.h>
#define BIT_FOR_PIXEL 24
#define BMP_TYPE 19778
#define PIXEL_SIZE sizeof(struct pixel)
#define PLANES 1
#define SIZE 40



inline static uint8_t get_padding(uint64_t width) {
    return (4 - ((PIXEL_SIZE * width) % 4)) % 4;
}

inline static size_t file_size(const struct image *image) {
    return (image->width  + get_padding(image->width)) * PIXEL_SIZE * image->height;
}

inline static struct bmp_header create_header(const struct image *image)
{
    return (struct bmp_header){
            .bfType = BMP_TYPE,
            .bfileSize = file_size(image) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = HEADER_SIZE,
            .biSize = SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_FOR_PIXEL,
            .biCompression = 0,
            .biSizeImage = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0};
}


enum read_status get_header(FILE* file, struct bmp_header *header) {
    if(fread(header, sizeof(struct bmp_header), 1, file) != 1){
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* image) {
    if (in == NULL) {
        return READ_INVALID_ARGS;
    }

    struct bmp_header bmp_header = {0};
    if (get_header(in, &bmp_header) != READ_OK)
    {
        return READ_INVALID_HEADER;
    }

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);

    if (fseek(in, (long) bmp_header.bOffBits, SEEK_SET)) {
        delete_image(image);
        return READ_INVALID_OFFBITS;
    }


    uint8_t padding = get_padding(image->width);

    for (size_t i = 0; i < image->height; ++i) {
        if (fread(image->data + i * image->width, sizeof(struct pixel), image->width, in) != image->width) {
            delete_image(image);
            return READ_INVALID_BITS;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            delete_image(image);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;

}

enum write_status to_bmp(FILE* out, const struct image* image) {
    if (image == NULL) {
        return WRITE_INVALID_ARGS;
    }
    struct bmp_header header = create_header(image);

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    uint8_t padding = get_padding(image->width);
    uint32_t padding_space = 0;

    for (size_t i = 0; i < image->height; ++i) {
        if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) != image->width) {
            return WRITE_ERROR;
        }


        if (fwrite(&padding_space, sizeof(int8_t), padding, out) != padding) {
            return WRITE_ERROR;
        }

    }

    return WRITE_OK;

}
