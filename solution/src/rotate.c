#include "image.h"
#include "rotate.h"
#include <stddef.h>

struct image rotate_by_90(struct image* image) {
    struct image rotated_image = create_image(image->height, image->width);
    set_pixels(rotated_image, *image->data);

    for (size_t y = 0; y < image->height; ++y) {
        for (size_t x = 0; x < image->width; ++x) {
            struct pixel current_pixel = image->data[y * image->width + x];
            rotated_image.data[(rotated_image.height - x - 1) * rotated_image.width + y] = current_pixel;
        }
    }
    delete_image(image);
    return rotated_image;
}

void rotate(struct image* image, int64_t angle) {
    angle = (angle + 360) % 360;
    for (int64_t i = 0; i * 90 < angle; i++) {
        *image = rotate_by_90(image);
    }
}
