#include <inttypes.h>

struct image rotate_by_90(struct image* image);

void rotate(struct image* image, int64_t angle);

static inline void set_pixels(struct image image, struct pixel data) {
    image.data = &data;
}

